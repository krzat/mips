﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MIPS.Debugger
{
    public class DumpGrid : DataGridView
    {
        string[] dump;
        public ICollection<int> Breakpoints { get; set; }

        public DumpGrid()
        {
            VirtualMode = true;
            CellValueNeeded += DumpGrid_CellValueNeeded;
            CellFormatting += DumpGrid_CellFormatting;
        }

        protected override void OnCellDoubleClick(DataGridViewCellEventArgs e)
        {
            var value = Rows[e.RowIndex].Cells[0].Value.ToString();
            if (value.StartsWith("  "))
            {
                var mem = Main.LineToAddress(value);

                if (Breakpoints.Contains(mem))
                    Breakpoints.Remove(mem);
                else
                    Breakpoints.Add(mem);
                InvalidateRow(e.RowIndex);
            }
        }

        void DumpGrid_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            var value = Rows[e.RowIndex].Cells[0].Value.ToString();
            var index = Main.LineToAddress(value);

            var breakpoint = Breakpoints.Contains(index);
            if (breakpoint) e.CellStyle.BackColor = Color.FromArgb(255, 255, 200, 200);
            //throw new NotImplementedException();
        }

        void DumpGrid_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            if (dump == null) return;
            e.Value = dump[e.RowIndex].Replace("\t", "    ");
        }

        protected override bool ProcessDataGridViewKey(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                return true;
            }
            return base.ProcessDataGridViewKey(e);
        }

        public string[] Dump
        {
            set
            {
                dump = value;
                RowCount = dump.Length;
                InvalidateColumn(0);
            }
        }

        protected override void OnSelectionChanged(EventArgs e)
        {
            return;
            if (index == 0 || CurrentCell == null) return;
            base.OnSelectionChanged(e);

            if (CurrentCell.RowIndex != index)
                SelectedIndex = index;
        }

        int index;
        public int SelectedIndex
        {
            set
            {
                index = value;
                CurrentCell = Rows[value].Cells[0];
            }
        }
    }
}
