﻿using MIPS.Architecture;
using MIPS.Graphical;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MIPS.Debugger
{
    public partial class Main : Form
    {
        Core core;
        Controller controller;
        AutoResetEvent stepEvent = new AutoResetEvent(false);
        string[] dump;

        string directory;

        HashSet<int> breakpoints;

        public Main(string dir)
        {
            InitializeComponent();

            breakpoints = new HashSet<int>();
            dgv.Breakpoints = breakpoints;

            directory = dir;
            core = new Core();
            core.Closed += core_Closed;
            core.Slave = true;
            controller = core.Controller;
            controller.ProgramDirectory = directory;
            core.Machine.Executing += Machine_Executing;

            dgv.Focus();

            Task.Run(() =>
            {
                BuildAndLoad();
                core.Run();
            });

        }

        void core_Closed()
        {
            Invoke(new Action(Close));
        }

        void dgv_CellValueNeeded(object sender, DataGridViewCellValueEventArgs e)
        {
            var line = dump[e.RowIndex];
            e.Value = line;
        }

        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
            stepEvent.Set();
        }

        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
        }

        int AddressToLine(int mem)
        {
            var format = string.Format("  {0:X6}", mem);

            for (int i = 0; i < dump.Length; i++)
            {
                if (dump[i].StartsWith(format, StringComparison.OrdinalIgnoreCase))
                    return i;
            }

            return -1;
        }

        public static int LineToAddress(string line)
        {
            if(line.StartsWith("  "))
            {
                return Convert.ToInt32(line.Substring(2, 6), 16);
            }
            return -1;
        }

        unsafe void SetLine(int mem)
        {
            var builder = new StringBuilder();
            var m = core.Machine;
            for (int i = 0; i < m.Registers.Length; i++ )
            {
                builder.AppendLine(string.Format("{0,-5} {1:X8} {1}", (Register)i, m.Registers[i]));
            }
            builder.AppendLine(string.Format("{0,-5} {1:X8} {1}", "High", m.High));
            builder.AppendLine(string.Format("{0,-5} {1:X8} {1}", "Low", m.Low));

            var index = AddressToLine(mem);

            
            Invoke(new Action(() =>
            {
                tbRegisters.Text = builder.ToString();
                dgv.SelectedIndex = index;
            }));
        }

        protected override void OnClosed(EventArgs e)
        {
            base.OnClosed(e);
            Pause();
        }

        void Machine_Executing(int mem)
        {
            if (breakpoints.Contains(mem))
                stepping = true;

            if(stepping)
            {
                SetLine(mem);
                stepEvent.WaitOne();
            }

        }

        void Pause()
        {
            controller.Stop(false);
            stepEvent.Set();
            controller.Stop();
            stepEvent.Reset();
        }

        void BuildAndLoad()
        {
            Pause();
            stepping = true;
            if (controller.BuildAndLoad())
            {
                dump = File.ReadAllLines(Loader.DirectoryToFile(controller.ProgramDirectory, ".txt"));
                Invoke(new Action(() =>
                {
                    breakpoints.Clear();
                    dgv.Dump = dump;
                }));
                controller.Start();
            }

        }



        private void buttonReset_Click(object sender, EventArgs e)
        {
            Task.Run(() => {
                Pause();
                stepping = true;
                controller.Reset();
                controller.Start();              
            });

        }

        bool stepping = true;
        private void buttonStep_Click(object sender, EventArgs e)
        {
            stepping = true;
            stepEvent.Set();
        }

        private void buttonRecompile_Click(object sender, EventArgs e)
        {
            Task.Run((Action)BuildAndLoad);
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            if(stepping)
            {
                stepping = false;
                stepEvent.Set();
            }

        }

    }
}
