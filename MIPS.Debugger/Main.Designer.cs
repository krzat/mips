﻿namespace MIPS.Debugger
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.buttonStep = new System.Windows.Forms.ToolStripButton();
            this.buttonReset = new System.Windows.Forms.ToolStripButton();
            this.buttonRecompile = new System.Windows.Forms.ToolStripButton();
            this.tbRegisters = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonRun = new System.Windows.Forms.ToolStripButton();
            this.dgv = new MIPS.Debugger.DumpGrid();
            this.columnInstruction = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStrip1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonStep,
            this.buttonReset,
            this.buttonRecompile,
            this.buttonRun});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(676, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // buttonStep
            // 
            this.buttonStep.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonStep.Image = ((System.Drawing.Image)(resources.GetObject("buttonStep.Image")));
            this.buttonStep.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonStep.Name = "buttonStep";
            this.buttonStep.Size = new System.Drawing.Size(34, 22);
            this.buttonStep.Text = "Step";
            this.buttonStep.Click += new System.EventHandler(this.buttonStep_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonReset.Image = ((System.Drawing.Image)(resources.GetObject("buttonReset.Image")));
            this.buttonReset.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(39, 22);
            this.buttonReset.Text = "Reset";
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonRecompile
            // 
            this.buttonRecompile.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonRecompile.Image = ((System.Drawing.Image)(resources.GetObject("buttonRecompile.Image")));
            this.buttonRecompile.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonRecompile.Name = "buttonRecompile";
            this.buttonRecompile.Size = new System.Drawing.Size(67, 22);
            this.buttonRecompile.Text = "Recompile";
            this.buttonRecompile.Click += new System.EventHandler(this.buttonRecompile_Click);
            // 
            // tbRegisters
            // 
            this.tbRegisters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbRegisters.Font = new System.Drawing.Font("Consolas", 9.25F);
            this.tbRegisters.Location = new System.Drawing.Point(3, 3);
            this.tbRegisters.Multiline = true;
            this.tbRegisters.Name = "tbRegisters";
            this.tbRegisters.ReadOnly = true;
            this.tbRegisters.Size = new System.Drawing.Size(173, 489);
            this.tbRegisters.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.61064F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.38936F));
            this.tableLayoutPanel1.Controls.Add(this.dgv, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tbRegisters, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(676, 495);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // buttonRun
            // 
            this.buttonRun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonRun.Image = ((System.Drawing.Image)(resources.GetObject("buttonRun.Image")));
            this.buttonRun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonRun.Name = "buttonRun";
            this.buttonRun.Size = new System.Drawing.Size(32, 22);
            this.buttonRun.Text = "Run";
            this.buttonRun.Click += new System.EventHandler(this.buttonRun_Click);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgv.Breakpoints = null;
            this.dgv.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.columnInstruction});
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(182, 3);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Consolas", 9.25F);
            this.dgv.RowTemplate.Height = 20;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(491, 489);
            this.dgv.TabIndex = 3;
            this.dgv.VirtualMode = true;
            // 
            // columnInstruction
            // 
            this.columnInstruction.HeaderText = "Instruction";
            this.columnInstruction.Name = "columnInstruction";
            this.columnInstruction.ReadOnly = true;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(676, 520);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.toolStrip1);
            this.KeyPreview = true;
            this.Name = "Main";
            this.Text = "MIPS Debugger";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton buttonReset;
        private System.Windows.Forms.TextBox tbRegisters;
        private System.Windows.Forms.ToolStripButton buttonStep;
        private System.Windows.Forms.ToolStripButton buttonRecompile;
        private DumpGrid dgv;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DataGridViewTextBoxColumn columnInstruction;
        private System.Windows.Forms.ToolStripButton buttonRun;
    }
}

