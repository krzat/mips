﻿#pragma once

#ifndef true
#define true 1
#endif

#ifndef false
#define false 0
#endif

void kprintf(const char* format, ...);
void panic(int code);
void halt();

