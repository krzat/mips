﻿
extern int main();
extern "C" void _init();

extern "C" void __start()
{	
	_init();
	main();
	
	// 'Exit' syscall.
	asm ("li $v0, 10\n\t"
	     "syscall"
		 : : : "v0"
		 );
}

void print(const char *str)
{
	asm volatile (
		 "li $v0, 4\n\t"
		 "lw $a0, %0\n\t"
		 "syscall"
		 : 
		 : "m" (str)
	     : "v0", "a0"
		 );
}

int getint()
{
	int result;

	// Get integer syscall
	asm ("li $v0, 5\n\t"
		 "syscall\n\t"
		 "addu %0, $0, $v0"
		 : "=r" (result)
		 : : "v0"
		 );

	return result;
}

void print_char(char c)
{
	char ch[2];
	ch[0] = c;
	ch[1] = '\0';

	print(ch);
}

void println(char *str)
{
	print(str);
	print("\n");
}

void printhexb(char c)
{
    char str[3] = {'0'+((c&0xF0)>>4),'0'+(c&0xF),0};
    if (str[0] > '9') str[0] += 7;
    if (str[1] > '9') str[1] += 7;
    print(str);
}

void printhexw(short w)
{
    printhexb(w>>8);
    printhexb(w);
}

void printhexd(int d)
{
    printhexw(d>>16);
    printhexw(d);
}

void printdec(int n)
{
	char buffer[50];
	int i = 0, t = 0;
	int isNeg = n<0;

	unsigned int n1 = isNeg ? -n : n;
	
	if(!n)
	{
		print("0");
		return;
	}

	while(n1!=0)
	{
		buffer[i++] = n1%10+'0';
		n1=n1/10;	
	}

	if(isNeg)
		buffer[i++] = '-';

	buffer[i] = '\0';
	
	for(t = 0; t < i/2; t++)
	{
		char swap = buffer[t];
		buffer[t] = buffer[i-t-1];
		buffer[i-t-1] = swap;
		//buffer[t] ^= buffer[i-t-1];
		//buffer[i-t-1] ^= buffer[t];
		//buffer[t] ^= buffer[i-t-1];
	}

	print(buffer);
}

void kprintf(const char* format, ...)
{
	unsigned *ptr = (unsigned*)(&format) + 1;
    while (*format)
    {
        
        // Skip 3 items on the stack at %ebp: prev %epb, %eip, and $format.
        if (*format == '%')
        {
			unsigned val = *ptr;
			ptr++;
            switch (*++format)
            {
                case 'd':
                    printdec((int)val);
                    break;
                case 's':
                    print((const char*)val);
                    break;
                case 'c':
                    print_char((char)val);
                    break;
                case 'b':
                    print("0x");
                    printhexb(val);
                    break;
                case 'w':
                    print("0x");
                    printhexw(val);
                    break;
                case 'l':
                    print("0x");
                    printhexd(val);
                    break;
                case '%':
                    print_char('%');
                    break;
                default:
                    print_char('%');
                    print_char(*format);
                    break;
            }
            
        }
        else
        {
            print_char(*format);
        }
        format++;

    }
}
