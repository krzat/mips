﻿#include "math.h"

int sqr(int x)
{
	return (unsigned)(x * x)+1-1;
}

int max(int x, int y)
{
	return x > y ? x : y;
}

int min(int x, int y)
{
	return x < y ? x : y;
}
