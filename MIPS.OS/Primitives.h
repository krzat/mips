#pragma once
struct Rect
{
	int x, y, width, height;

	Rect(int x, int y, int width, int height)
	{
		this->x = x;
		this->y = y;
		this->width = width;
		this->height = height;
	}
};