#pragma once
#include "ports.h"
#include "system.h"

class Keyboard
{
	char prev;

public:

	Keyboard()
	{
	}

	char __attribute__((optimize("O0"))) readKey() {
		return portKeyboard;
	}

	char readChar() {
		char prev = readKey();
		while (true) {
			char c = readKey();
			if (c != '\0' && c != prev)
			{
				return c;
			}
			prev = c;
		}
	}

	void readString(char* buffer, int length)
	{
		for (int i = 0; i < length; i++) {
			char c = readChar();
			if (c == '\n') {
				buffer[i] = '\0';
				return;
			}
			buffer[i] = c;
		}
	}
};

Keyboard keyboard;