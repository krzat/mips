#pragma once
#include "Primitives.h"
#include "math.h"

class Image
{
public:
	int* data;
	int width;
	int height;

	void draw(Image &img, Rect src, Rect dst)
	{
		for (int x = 0; x < src.width; x++)
			for (int y = 0; y < src.height; y++)
			{
				set(x + dst.x, y + dst.y, img.get(x + src.x, y + src.y));
			}
	}

	int get(int x, int y)
	{
		return data[x + y * width];
	}

	void set(int x, int y, int color)
	{
		data[x + y * width] = color;
	}

	void drawRectangle(Rect rect, int color)
	{


		for (int x = 0; x < rect.width; x++)
			for (int y = 0; y < rect.height; y++) 
			{
				set(x + rect.x, y + rect.y, color);
			}
	}

	void drawCircle(int x, int y, int radius, unsigned color)
	{
		int X, Y;
		int start_x = max(0, x - radius);
		int start_y = max(0, y - radius);
		int end_x = min(width, start_x + radius * 2);
		int end_y = min(height, start_y + radius * 2);
		int r2 = sqr(radius);

		for (X = start_x; X < end_x; X++)
		for (Y = start_y; Y < end_y; Y++)
		if ((sqr(X - x) + sqr(Y - y)) < r2)
			data[X + Y*width] = color;
	}

	void clear(int color)
	{
		for (int i = 0; i < width*height; i++)
			data[i] = color;
	}
};