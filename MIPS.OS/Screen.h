#pragma once
#include "Image.h"
#include "ports.h"

class Screen : public Image
{
public:
	Screen() 
	{
		width = SCREEN_WIDTH;
		height = SCREEN_HEIGHT;
		data = &portVideo[0];
	}
	void display()
	{
		asm volatile (
		"li $v0, 100\n\t"
		"syscall\n\t"
		);
	}
};

Screen screen;
