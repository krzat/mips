#pragma once
#include "Image.h"
#include "Screen.h"
#include "Keyboard.h"

#define FONT_SIZE 15

int font_data[FONT_SIZE * FONT_SIZE* 256];
class Console
{
	Image font;
	int x, y;
	bool autoDisplay;

public:
	Console()
	{
		font.data = font_data;
		font.width = FONT_SIZE * 16;
		font.height = FONT_SIZE * 16;		
		autoDisplay = true;
	}

	void readLine(char *buffer, int length)
	{
		int i = 0;
		draw('_');
		while (true)
		{
			char c = keyboard.readChar();
			if (c == '\r')
			{
				draw(' ');
				nextLine();
				break;
			}
			else if (c == '\b')
			{
				if (i > 0)
				{
					i--;
					draw(' ');
					moveLeft();
					draw('_');
				}
			}
			else
			{
				write(c);
				draw('_');
				buffer[i++] = c;
				
				if (i >= length - 1)
					break;
			}
			buffer[i] = '\0';

		}

	}

	void moveRight()
	{
		x++;
		if (x >= SCREEN_WIDTH / FONT_SIZE)
			nextLine();
	}

	void moveLeft()
	{
		x--;
		if (x < 0)
		{
			x = SCREEN_WIDTH / FONT_SIZE - 1;
			y--;
			if (y < 0) y = 0;
		}
	}

	void nextLine()
	{
		x = 0;
		y++;
		if (y >= SCREEN_HEIGHT / FONT_SIZE)
			y = 0;
	}

	void draw(char c)
	{
		Rect src(0, 0, FONT_SIZE, FONT_SIZE), dst(0, 0, FONT_SIZE, FONT_SIZE);
		src.x = c % 16 * FONT_SIZE;
		src.y = c / 16 * FONT_SIZE;
		dst.x = x * FONT_SIZE;
		dst.y = y * FONT_SIZE;
		screen.draw(font, src, dst);
		if(autoDisplay) 
			screen.display();
	}

	void write(char c)
	{
		if (c == '\n')
		{
			x = 0;
			y++;
			if (y > SCREEN_HEIGHT / FONT_SIZE)
				y = 0;
			return;
		}

		draw(c);
		
		moveRight();

	}
	void write(const char* str)
	{
		bool enabled = autoDisplay;
		if(enabled) autoDisplay = false;
		while (*str)
		{
			write(*str);
			str++;
		}
		if(enabled) autoDisplay = true;
	}
};

Console console;
