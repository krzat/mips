#pragma once

#define SCREEN_WIDTH 640
#define SCREEN_HEIGHT 360
#define FONT_SIZE 15

//pseudo mapped memory for outside data
int portVideo[SCREEN_HEIGHT * SCREEN_WIDTH];
int portFont[FONT_SIZE * 16 * 16];
volatile char portKeyboard;
