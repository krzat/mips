﻿#include "Console.h"
#include "Screen.h"
#include "Keyboard.h"

#define WHITE 0xFFFFFFFF
#define BLACK 0x00000000
int main()
{
	int radius = 300;
	unsigned color = WHITE;

	char data[128];

	screen.drawCircle(screen.width / 2, screen.height / 2, 160, 0xFF000033);
	screen.drawCircle(screen.width / 2, screen.height / 2, 40, 0xFF000055);

	console.write("Hello, Dave. You're looking well today.\n");

	while (true)
	{
		console.readLine(data, 128);
		console.write(data);
		console.write('\n');
	}
	return 1;
}
