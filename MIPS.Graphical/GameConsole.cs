﻿using SFML.Graphics;
using SFML.Window;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIPS.Graphical
{
    class GameConsole : Drawable
    {
        class MyWriter : TextWriter
        {
            private GameConsole console;

            public MyWriter(GameConsole gameConsole)
            {
                // TODO: Complete member initialization
                this.console = gameConsole;
            }
            public override void Write(char value)
            {
                if (value == '\r')
                    return;

                console.queue.Enqueue(value);
            }

            public override Encoding Encoding
            {
                get { return Encoding.ASCII; }
            }
        }

        class MyReader : TextReader
        {
            private GameConsole gameConsole;

            public MyReader(GameConsole gameConsole)
            {
                // TODO: Complete member initialization
                this.gameConsole = gameConsole;
            }
            public override int Read(char[] buffer, int index, int count)
            {
                return base.Read(buffer, index, count);
            }
        }

        Font font;
        List<Text> lines;
        ConcurrentQueue<char> queue = new ConcurrentQueue<char>();

        void AddLine()
        {
            Text removed = null;
            if (lines.Count > 10)
            {
                removed = lines[0];
                removed.DisplayedString = "";
                lines.RemoveAt(0);
            }

            var line = removed ?? new Text("", font, 14);
            lines.Add(line);

            Vector2f pos = new Vector2f(10, 10);
            foreach (var obj in lines)
            {
                obj.Position = pos;
                pos.Y += 16;
            }
        }

        public bool Visible = true;

        public GameConsole()
        {
            Console.SetOut(new MyWriter(this));
            Console.SetIn(new MyReader(this));

            font = new Font("Content/verdana.ttf");
            lines = new List<Text>();
            AddLine();
        }

        public void Clear()
        {
            lines.Clear();
            AddLine();
        }

        StringBuilder buffer = new StringBuilder();

        void Dequeue()
        {
            while (queue.Count > 0)
            {
                char value;
                if (queue.TryDequeue(out value))
                {
                    if (value == '\n')
                    {
                        lines.Last().DisplayedString += buffer;
                        buffer.Clear();
                        AddLine();
                    }
                    else
                        buffer.Append(value);
                }
            }

            if (buffer.Length > 0)
            {
                lines.Last().DisplayedString += buffer;
                buffer.Clear();
            }
        }

        RenderStates states;

        public void Draw(RenderTarget target, RenderStates states)
        {
            if (queue.Count > 0)
            {
                Dequeue();

            }
            if(Visible)
                foreach (var line in lines)
                    target.Draw(line);
        }
    }
}
