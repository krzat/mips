﻿using SFML.Utils;
using SFML.Window;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MIPS.Graphical
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length != 1)
                throw new ArgumentException();
            var path = args[0];

            var core = new Core();
            core.Slave = false;
            core.Controller.ProgramDirectory = path;
            core.Run();

            core.Dispose();
        }
    }
}
