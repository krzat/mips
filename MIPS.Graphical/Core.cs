﻿using SFML.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MIPS.Architecture;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using SFML.Graphics;
using System.Threading.Tasks;
using SFML.Window;


namespace MIPS.Graphical
{
    public class Core : Game
    {
        const int ScreenWidth = 640, ScreenHeight = 360, ScreenZoom = 2;

        Sprite screen;
        bool updateVideo;
        byte[] buffer = new byte[ScreenHeight * ScreenWidth * 4];

        public event Action Closed;
        public CPU Machine { get; private set; }
        public Controller Controller { get; private set; }
        GameConsole console;
        bool busy;

        int portVideo;
        int portKeyboard;

        public bool Slave;

        public Core()
        {
            Machine = new CPU();
            Controller = new Architecture.Controller(Machine);
            Controller.ProgramLoaded += Controller_Loaded;
            Machine.OnSystemCall += HandleSystemCall;

            Stopwatch watch = new Stopwatch();
            Controller.Starting += () => { watch.Restart(); };
            Controller.Stopping += () => { 
                watch.Stop();
                Console.WriteLine("CPU ran: {0} ms", watch.ElapsedMilliseconds);
            };
        }

        public void Run()
        {
            var settings = new GameSettings();
            settings.Title = "MIPS.net";
            settings.Width = ScreenWidth*2;
            settings.Height = ScreenHeight*2;
            settings.AntialiasingLevel = 4;
            settings.Style = Styles.Close;
            settings.FramerateLimit = 60;
            Run(settings);
        }

        protected unsafe override void Initialize()
        {           
            console = new GameConsole();
            console.Visible = false;
            Console.WriteLine("Press F5 to reset.");
            Console.WriteLine("Press F6 to recompile and load");
            Console.WriteLine("Press ~ to hide/show console");

            screen = new Sprite(new Texture(ScreenWidth,ScreenHeight));
            screen.Scale = new Vector2f(2, 2);
            Window.Closed += Window_Closed;
            Window.KeyPressed += Window_KeyPressed;
            Window.KeyReleased += Window_KeyReleased;
            Window.TextEntered += Window_TextEntered;

            if (!Slave)
            {
                if (Controller.BuildAndLoad())
                {
                    Controller.Start();
                }
            }
        }

        protected override void Draw(float dT)
        {
            if (updateVideo)
            {
                screen.Texture.Update(buffer);
                updateVideo = false;
            }
            Window.Draw(screen);
            Window.Draw(console);
        }

        public void Dispose()
        {
            Controller.Dispose();
        }

        unsafe void bubble(int[] data, int count)
        {
	        bool done = false;
	        while (!done)
	        {
		        done = true;
		        for (int i = 0; i < count-1; i++) {
			        if (data[i] > data[i + 1])
			        {
				        int swap = data[i];
				        data[i] = data[i + 1];
				        data[i + 1] = swap;
				        done = false;
			        }
		        }
	        }

        }

        void Controller_Loaded()
        {
            portKeyboard = Controller.Program.FindSymbol("portKeyboard");
            portVideo = Controller.Program.FindSymbol("portVideo");

            if(portVideo >= 0)
            {
                var rand = new Random();
                rand.NextBytes(buffer);
                Buffer.BlockCopy(buffer, 0, Machine.Memory, portVideo, buffer.Length);
            }



            var ar = new int[buffer.Length/4];
            Buffer.BlockCopy(buffer,0, ar, 0, buffer.Length);
            var watch = Stopwatch.StartNew();
            Array.Sort(ar);
            watch.Stop();
            Console.WriteLine(watch.ElapsedMilliseconds);

            var mem = Controller.Program.FindSymbol("font_data");
            if (mem == -1) return;
            var data = new Image("Content/15x15.png");
            var pixels = data.Pixels;
            Buffer.BlockCopy(pixels, 0, Machine.Memory, mem, pixels.Length);
            data.Dispose();

        }

        void Window_TextEntered(object sender, TextEventArgs e)
        {
            if (portKeyboard != -1)
                Machine.Memory[portKeyboard] = (byte)e.Unicode[0];
        }

        void Window_KeyReleased(object sender, KeyEventArgs e)
        {
            if(portKeyboard != -1)
                Machine.Memory[portKeyboard] = 0;
        }

        void Window_Closed(object sender, EventArgs e)
        {
            if (Closed != null)
                Closed();
            Dispose();
        }

        void Window_KeyPressed(object sender, SFML.Window.KeyEventArgs e)
        {
            if (e.Code == Keyboard.Key.F1)
            {
                console.Visible = !console.Visible;
                screen.Color = console.Visible ? new Color(150,150,150) : Color.White;
            }

            if (Slave || busy) return;
            
            if(e.Code == Keyboard.Key.F5)
            {
                Task.Run(() => {
                    Controller.Reset();
                    Controller.Start();
                    busy = false;
                });
            }

            if(e.Code == Keyboard.Key.F6)
            {
                Task.Run(() =>
                {
                    if(Controller.BuildAndLoad())
                        Controller.Start();
                    busy = false;
                });
            }
        }

        unsafe void HandleSystemCall(CPU sender, int code)
        {
            var RF = Machine.Registers;

            switch(code)
            {
                // Print Integer, a0 = number to be printed
                case 1:
                    var number = ((int)RF[(int)Register.a0]).ToString();
                    Console.Write(number);
                    break;
                // Print Float, a0 = number to be printed
                case 2:
                    throw new NotImplementedException();
                // Print Double, a0 = number to be printed
                case 3:
                    throw new NotImplementedException();
                // Print String, a0 = address of string in memory
                case 4:
                    var addr = RF[(int)Register.a0];
                    //Console.WriteLine("From address: {0:X6}", addr);
                    var result = "";
                    while (true)
                    {
                        byte b = Machine.GetByte(addr);

                        if (b == 0)
                            break;
                        if (b == 10)
                            result += "\r\n";
                        else
                            result += (char)b;
                        addr++;
                    }
                    Console.Write(result);
                    break;
                // Read Integer, Number returned in v0
                case 5:
                    RF[(int)Register.v0] = 0;
                    return;
                // Read Float, Number returned in f0
                case 6:
                    throw new NotImplementedException();
                // Read Double, Number returned in f0
                case 7:
                    throw new NotImplementedException();
                // Read String, a0 = address of input buffer in memory, a1 = length of buffer (n)
                case 8:
                    throw new NotImplementedException();
                // Sbrk, a0 = amount, address returned in v0
                case 9:
                    throw new NotImplementedException();
                // Exit
                case 100:
                    if(portVideo != -1)
                    {
                        Buffer.BlockCopy(Machine.Memory, portVideo, buffer, 0, buffer.Length);
                        updateVideo = true;
                    }
                    break;
            }
        }
    }
}
