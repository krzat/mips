﻿using ELFSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MIPS.Architecture
{


    public class Controller
    {
        Thread worker;
        public CPU Machine { get; private set; }
        volatile bool disposing, shouldRun, isRunning;

        public event Action Starting, Stopping, Building, ProgramLoaded;

        public bool IsRunning { get { return isRunning; } }

        public ELF<uint> Program { get; private set; }

        public string ProgramDirectory;

        ManualResetEvent enabled  = new ManualResetEvent(false);

        public Controller(CPU machine)
        {
            worker = new Thread(Run);
            worker.Name = "CPU thread";
            this.Machine = machine;
            Machine.OnSystemCall += Machine_OnSystemCall;
            worker.Start();

            Starting += ()=> {Console.WriteLine("CPU starting...");};
            Stopping += ()=> {Console.WriteLine("CPU stopping...");};
        }

        void Machine_OnSystemCall(CPU arg1, int arg2)
        {
            if (arg2 == 10)
                Stop(false);
        }

        void Run()
        {
            while(true)
            {
                isRunning = false;
                enabled.WaitOne();
                if (disposing)
                    return;

                isRunning = true;               
                if (Starting != null) Starting();


                while (shouldRun)
                    Machine.Step();
                //try
                //{
                //    while (shouldRun)
                //        Machine.Step();
                //}
                //catch(Exception e)
                //{
                //    Stop();
                //    Console.WriteLine(e.Message);
                //    throw e;
                //}

                if (Stopping != null) Stopping();
            }

        }

        public void Start()
        {
            shouldRun = true;
            enabled.Set();
            while (!isRunning)
            {
            }
        }

        public bool BuildAndLoad()
        {
            Stop();
            Machine.Clear();
            Console.WriteLine("\nBuilding and loading...");
            if (Program != null) Program.Dispose();

            Program = Loader.BuildAndLoad(Machine, ProgramDirectory);
            if (Program != null && ProgramLoaded != null) ProgramLoaded();
            return Program != null;
        }

        public void Reset()
        {
            Console.WriteLine("\nResetting...");

            var running = IsRunning;
            if(running) Stop();
            
            Machine.Clear();
            if (Program != null)
            {
                Loader.Load(Machine, Program);
                if (ProgramLoaded != null) ProgramLoaded();
            }
            if (running) Start();
        }

        public void Stop(bool ensure = true)
        {
            enabled.Reset();
            shouldRun = false;

            if(ensure)
                while (isRunning) ;
        }

        public void Dispose()
        {
            disposing = true;
            shouldRun = false;
            enabled.Set();
        }
    }
}
