﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MIPS.Architecture;
using System.IO;
using System.Configuration;
using System.Diagnostics;
using ELFSharp;
using ELFSharp.Segments;
using ELFSharp.Sections;



namespace MIPS.Architecture
{
    public static class Loader
    {
        public class GCCCompileError : Exception
        {
            public GCCCompileError(string message) : base(message) { }
        }

        public static int FindSymbol(this ELF<uint> elf, string symbol)
        {
            var symtab = elf.Sections.First(x => x.Name == ".symtab") as ELFSharp.Sections.ISymbolTable;
            var entry = symtab.Entries.FirstOrDefault(x => x.Name == symbol) as SymbolEntry<uint>;
            if (entry == null) return -1;
            return (int)entry.Value;
        }

        static bool ExecuteTool(string parameters)
        {
            var result = true;
            var exe = ConfigurationManager.AppSettings["compiler"] ?? "mips-sde-elf-gcc";
            var flags = ConfigurationManager.AppSettings["flags"] ?? "-mno-check-zero-division -mips1 -O3";
            //-mno-check-zero-division -fno-delayed-branch -O3

            var psi = new ProcessStartInfo(exe, parameters + " " +flags);

            psi.RedirectStandardOutput = true;
            psi.RedirectStandardError = true;
            psi.UseShellExecute = false;
            psi.CreateNoWindow = true;

            var p = Process.Start(psi);

            //Console.Write(p.StandardOutput.ReadToEnd());
            string error;
            while((error = p.StandardError.ReadLine()) != null)
            {
                Console.WriteLine(error);
                if (!error.Contains("warning:") && !error.Contains("In function"))
                    result = false;
            }

            p.WaitForExit();
            return result;
        }

        static void ExecuteObjDump(string elfFile)
        {
            var info = new ProcessStartInfo("mips-sde-elf-objdump", "-S "+elfFile);
            info.RedirectStandardOutput = true;
            info.UseShellExecute = false;
            info.CreateNoWindow = true;

            var p = Process.Start(info);
            File.WriteAllText(Path.ChangeExtension(elfFile, ".txt"), p.StandardOutput.ReadToEnd());
        }

        public static ELF<uint> Build(string dir)
        {
            if (!Directory.Exists(dir))
                throw new ArgumentException();
            //var files = Directory.GetFiles(dir, "*.c", SearchOption.AllDirectories);
            var files = from d in Directory.EnumerateFiles(dir, "*.*", SearchOption.AllDirectories)
                        let ext = Path.GetExtension(d)
                        where ext == ".c" || ext == ".cpp"
                        select d;

            var ar = files.ToArray();

            var program = DirectoryToFile(dir, ".elf");

            var args = string.Format(" -EL -o {0} {1}", program, string.Join(" ", files));
            var result = ExecuteTool(args);
            if(result)
            {
                ExecuteObjDump(program);
                return ELFReader.Load(DirectoryToFile(dir, ".elf")) as ELF<uint>;
            }
            return null;
        }

        public unsafe static void Load(CPU m, IELF obj)
        {
            var elf = (ELF<uint>)obj;
            if (elf.Endianess == Endianess.BigEndian)
                throw new NotImplementedException();

            foreach (var header in elf.Segments.Where(ph => ph.Type == SegmentType.Load))
            {
                var data = header.GetContents();
                Buffer.BlockCopy(data, 0, m.Memory, (int)header.Address, data.Length);
            }

            var symtab = elf.Sections.First(x => x.Name == ".symtab") as ELFSharp.Sections.ISymbolTable;
            var gp = symtab.Entries.First(x => x.Name == "_gp") as ELFSharp.Sections.SymbolEntry<uint>;

            var zz = symtab.Entries.First(x => x.Name.Contains("font"));

            m.Registers[(int)Register.gp] = (int)gp.Value;
            m.PC = (int)elf.EntryPoint;

            //var sdata = elf.Sections.First(x => x.Name == ".sdata").GetContents();
            //var address = BitConverter.ToInt32(sdata, 4);
            //Console.WriteLine("Found at: {0}", address);
        }

        public static ELF<uint> BuildAndLoad(CPU Machine, string dir)
        {
            if (!Directory.Exists(dir))
                throw new ArgumentException();
            var elf = Build(dir);
            if (elf != null)
                Load(Machine, elf);
            return elf;
        }

        public static string DirectoryToFile(string directory, string extension)
        {
            var info = new DirectoryInfo(directory);
            return Path.Combine(info.Parent.FullName, Path.ChangeExtension(info.Name, extension));
        }
    }
}
