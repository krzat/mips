﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;
using System.Diagnostics;

namespace MIPS.Architecture
{
    public unsafe class CPU
    {
        public byte[] Memory { get; private set; }
        public int[] Registers { get; private set; }
        public int PC;
        public int High;
        public int Low;


        GCHandle memoryHandle, registerHandle;
        unsafe int* RF;
        unsafe uint* uRF;
        public byte* memory;

        Instruction IR;

        public event Action<int> Executing;
        public event Action<CPU, int> OnSystemCall;

        public CPU()
        {
            Memory = new byte[256 * 1024 * 1024];
            Registers = new int[32];

            memoryHandle = GCHandle.Alloc(Memory, GCHandleType.Pinned);
            registerHandle = GCHandle.Alloc(Registers, GCHandleType.Pinned);

            memory = (byte*)memoryHandle.AddrOfPinnedObject();
            RF = (int*)registerHandle.AddrOfPinnedObject();
            uRF = (uint*)RF;

            Clear();
        }

        public unsafe void Clear()
        {
            Array.Clear(Registers, 0, 32);
            Array.Clear(Memory, 0, Memory.Length);

            High = 0;
            Low = 0;
            PC = 0;
            RF[(int)Register.sp] = 0x300001;
            nPC = 0;
            branchSlot = false;
        }

        int GetWord(int index)
        {
            return *(int*)(memory + index);
            
        }

        void SetWord(int index, int value)
        {
            *(int*)(memory + index) = value;
        }

        ushort GetShort(int index)
        {
            return *(ushort*)(memory + index);
        }
        void SetShort(int index, ushort value)
        {
            *(ushort*)(memory + index) = value;
        }

        public byte GetByte(int index)
        {
            return memory[index];
        }
        void SetByte(int index, byte value)
        {
            memory[index] = value;
        }

        int nPC = 0;

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public unsafe void Step()
        {
            var pc = unchecked((int)PC);
            var word = GetWord(pc);

            if (Executing != null)
                Executing(pc);

            IR.Decode((uint)word);

            switch (IR.OpCode)
            {
                #region R-type
                case OpCode.Register:
                    switch (IR.FunctionCode)
                    {
                        // Shift Left Logical
                        case FunctionCode.sll:
                            uRF[IR.Rd] = uRF[IR.Rt] << IR.sa;
                            break;
                        // Shift Right Logical
                        case FunctionCode.srl:
                            uRF[IR.Rd] = uRF[IR.Rt] >> IR.sa;
                            break;
                        // Shift Right Arithmetic
                        case FunctionCode.sra:
                            RF[IR.Rd] = RF[IR.Rt] >> IR.sa;
                            break;
                        // Shift Left Logical Variable
                        case FunctionCode.sllv:
                            uRF[IR.Rd] = uRF[IR.Rt] << RF[IR.Rs];
                            break;
                        // Shift Right Logical Variable
                        case FunctionCode.srlv:
                            uRF[IR.Rd] = uRF[IR.Rt] >> RF[IR.Rs];
                            break;
                        // Shift Right Arithmetic Variable
                        case FunctionCode.srav:
                            RF[IR.Rd] = RF[IR.Rt] >> RF[IR.Rs];
                            break;
                        // Jump Register
                        case FunctionCode.jr:
                            nPC = RF[IR.Rs];
                            break;
                        // Jump and Link Register
                        case FunctionCode.jalr:
                            RF[IR.Rd] = (int)(PC + 4);
                            nPC = RF[IR.Rs] - 4;
                            break;
                        // System Call
                        case FunctionCode.syscall:
                            if (OnSystemCall != null)
                                OnSystemCall(this, (int)RF[(int)Register.v0]);
                            break;
                        // Move From High
                        case FunctionCode.mfhi:
                            RF[IR.Rd] = High;
                            break;
                        // Move to High
                        case FunctionCode.mthi:
                            High = RF[IR.Rs];
                            break;
                        // Move From Low
                        case FunctionCode.mflo:
                            RF[IR.Rd] = Low;
                            break;
                        // Move To Low
                        case FunctionCode.mtlo:
                            Low = RF[IR.Rs];
                            break;
                        // Multiply
                        case FunctionCode.mult:
                            {
                                long result = (long)RF[IR.Rs] * (long)RF[IR.Rt];
                                Low = (int)result;
                                High = (int)(result >> 32);
                                break;
                            }
                        // Multiply Unsigned
                        case FunctionCode.multu:
                            {
                                ulong result = (ulong)uRF[IR.Rs] * (ulong)uRF[IR.Rt];
                                Low = (int)result;
                                High = (int)(result >> 32);
                                break;
                            }
                        // Divide
                        case FunctionCode.div:
                            {
                                long result;
                                Low = (int)Math.DivRem(RF[IR.Rs], RF[IR.Rt], out result);
                                High = (int)result;
                                break;
                            }
                        // Divide Unsigned
                        case FunctionCode.divu:
                            {
                                long result;
                                Low = (int)Math.DivRem(uRF[IR.Rs], uRF[IR.Rt], out result);
                                High = (int)result;
                                break;
                            }
                        // Add
                        case FunctionCode.add:
                            RF[IR.Rd] = RF[IR.Rs] + RF[IR.Rt];
                            // TODO: Generate exception in case of overflow.
                            break;
                        // Add Unsigned
                        case FunctionCode.addu:
                            uRF[IR.Rd] = uRF[IR.Rs] + uRF[IR.Rt];
                            break;
                        // Subtract
                        case FunctionCode.sub:
                            RF[IR.Rd] = RF[IR.Rs] - RF[IR.Rt];
                            break;
                        // Subtract Unsigned
                        case FunctionCode.subu:
                            uRF[IR.Rd] = uRF[IR.Rs] - uRF[IR.Rt];
                            break;
                        // And
                        case FunctionCode.and:
                            RF[IR.Rd] = RF[IR.Rs] & RF[IR.Rt];
                            break;
                        // Or
                        case FunctionCode.or:
                            RF[IR.Rd] = RF[IR.Rs] | RF[IR.Rt];
                            break;
                        // Exclusive Or
                        case FunctionCode.xor:
                            RF[IR.Rd] = RF[IR.Rs] ^ RF[IR.Rt];
                            break;
                        // Nor
                        case FunctionCode.nor:
                            RF[IR.Rd] = ~(RF[IR.Rs] | RF[IR.Rt]);
                            break;
                        // Set on Less Than
                        case FunctionCode.slt:
                            if (RF[IR.Rs] < RF[IR.Rt])
                                RF[IR.Rd] = 1;
                            else
                                RF[IR.Rd] = 0;
                            break;
                        // Set on Less Than Unsigned
                        case FunctionCode.sltu:
                            if (RF[IR.Rs] < RF[IR.Rt])
                                RF[IR.Rd] = 1;
                            else
                                RF[IR.Rd] = 0;
                            break;
                        default:
                            throw new NotImplementedException(string.Format("{0:X} at {1:X6}", IR.FunctionCode, PC));
                    }
                    break;
                #endregion
                #region I-type
                // Special branches with a branch code.
                case OpCode.Branch:
                    switch (IR.BranchCode)
                    {
                        // Branch if Greater Than or Equal to Zero
                        case BranchCode.bgez:
                            if (RF[IR.Rs] >= 0)
                                nPC = PC+ IR.SignExtendedImmediate * 4;
                            break;
                        // Branch if Greater Than or Equal to Zero, and Link
                        case BranchCode.bgezal:
                            if (RF[IR.Rs] >= 0)
                            {
                                RF[(int)Register.ra] = (int)(PC + 4);
                                nPC = PC + IR.SignExtendedImmediate * 4;
                            }
                            break;
                        // Branch if Less than Zero
                        case BranchCode.bltz:
                            if ((int)RF[IR.Rs] < 0)
                                nPC = PC + IR.SignExtendedImmediate * 4;
                            break;
                        // Branch if Less than Zero, and Link
                        case BranchCode.bltzal:
                            if ((int)RF[IR.Rs] < 0)
                            {
                                RF[(int)Register.ra] = (int)(PC + 4);
                                nPC = PC + IR.SignExtendedImmediate * 4;
                            }
                            break;
                        default:
                            // TODO: Invalid expression instruction?
                            throw new NotImplementedException();
                    }
                    break;
                // Branch if Equal
                case OpCode.beq:
                    if (RF[IR.Rs] == RF[IR.Rt])
                        nPC = PC + IR.SignExtendedImmediate * 4;
                    break;
                // Branch if Not Equal
                case OpCode.bne:
                    if (RF[IR.Rs] != RF[IR.Rt])
                        nPC = PC + IR.SignExtendedImmediate * 4;
                    break;
                // Branch if Less Than or Equal to Zero
                case OpCode.blez:
                    if ((int)RF[IR.Rs] <= 0)
                        nPC = PC + IR.SignExtendedImmediate * 4;
                    break;
                // Branch if Greater than Zero
                case OpCode.bgtz:
                    if ((int)RF[IR.Rs] > 0)
                        nPC = PC + IR.SignExtendedImmediate * 4;
                    break;
                // Add Immediate
                case OpCode.addi:
                    // TODO: Overflow exception
                    RF[IR.Rt] = RF[IR.Rs] + (IR.SignExtendedImmediate);
                    break;
                // Add Immediate Unsigned
                case OpCode.addiu:
                    RF[IR.Rt] = RF[IR.Rs] + IR.SignExtendedImmediate;
                    break;
                // Set on Less Than Immediate
                case OpCode.slti:
                    if (RF[IR.Rs] < IR.SignExtendedImmediate)
                        RF[IR.Rt] = 1;
                    else
                        RF[IR.Rt] = 0;
                    break;
                // Set on Less Than Immediate Unsigned
                case OpCode.sltiu:
                    if (RF[IR.Rs] < IR.Immediate)
                        RF[IR.Rt] = 1;
                    else
                        RF[IR.Rt] = 0;
                    break;
                // And Immediate
                case OpCode.andi:
                    RF[IR.Rt] = RF[IR.Rs] & IR.Immediate;
                    break;
                // Or Immediate
                case OpCode.ori:
                    RF[IR.Rt] = RF[IR.Rs] | IR.Immediate;
                    break;
                // Xor Immediate
                case OpCode.xori:
                    RF[IR.Rt] = RF[IR.Rs] ^ IR.Immediate;
                    break;
                // Load Upper Immediate
                case OpCode.lui:
                    RF[IR.Rt] = (int)IR.Immediate << 16;
                    break;
                // Misc. System Instructions
                case OpCode.System:
                    Debugger.Break();
                    break;
                    //throw new NotImplementedException();
                // Load Byte
                case OpCode.lb:
                    RF[IR.Rt] = (sbyte)GetByte(RF[IR.Rs] + IR.SignExtendedImmediate);
                    break;
                // Load Halfword
                case OpCode.lh:
                    RF[IR.Rt] = (short)GetShort(RF[IR.Rs] + IR.SignExtendedImmediate);
                    break;
                // Load Word Left
                case OpCode.lwl:
                    throw new NotImplementedException();
                // Load Word
                case OpCode.lw:
                    RF[IR.Rt] = GetWord(RF[IR.Rs] + IR.SignExtendedImmediate);
                    break;
                // Load Byte Unsigned
                case OpCode.lbu:
                    uRF[IR.Rt] = GetByte(RF[IR.Rs] + IR.SignExtendedImmediate);
                    break;
                // Load Halfword Unsigned
                case OpCode.lhu:
                    uRF[IR.Rt] = GetShort(RF[IR.Rs] + IR.SignExtendedImmediate);
                    break;
                // Load Word Right
                case OpCode.lwr:
                    throw new NotImplementedException();
                // Store Byte
                case OpCode.sb:
                    SetByte(RF[IR.Rs] + IR.SignExtendedImmediate, (byte)RF[IR.Rt]);
                    break;
                // Store Halfword
                case OpCode.sh:
                    SetShort(RF[IR.Rs] + IR.SignExtendedImmediate, (ushort)RF[IR.Rt]);
                    break;
                // Store Word Left
                case OpCode.swl:
                    throw new NotImplementedException();
                // Store Word
                case OpCode.sw:
                    SetWord(RF[IR.Rs] + IR.SignExtendedImmediate, RF[IR.Rt]);
                    break;
                // Store Word Right
                case OpCode.swr:
                    throw new NotImplementedException();
                #endregion
                #region J-type
                // The Jump instruction
                case OpCode.j:
                    nPC = (IR.Target << 2) - 4;
                    break;
                // The Jump and Link (call procedure) instruction
                case OpCode.jal:
                    RF[(int)Register.ra] = (PC + 4);
                    nPC = (IR.Target << 2) - 4;
                    break;
                #endregion
                default:
                    throw new NotImplementedException();
            }

            PC+= 4;

            if(nPC != 0)
            {
                if(branchSlot)
                {
                    PC = nPC + 4;
                    branchSlot = false;
                    nPC = 0;
                }
                else
                    branchSlot = true;
            }
        }

        bool branchSlot;


    }
}
