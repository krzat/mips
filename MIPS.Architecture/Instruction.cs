﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace MIPS.Architecture
{
    public struct Instruction
    {
        /// <summary>
        /// The OpCode of the instruction.
        /// </summary>
        public OpCode OpCode;

        /// <summary>
        /// The first source operand register for register- or immediate-format instructions.
        /// </summary>
        public int Rs;

        /// <summary>
        /// The second source operand register for register- or immediate-format instructions.
        /// </summary>
        public int Rt;

        public BranchCode BranchCode { get { return (BranchCode)Rt; } set { Rt = (int)value; } }

        /// <summary>
        /// The destination register for a register-based instruction.
        /// </summary>
        public int Rd;

        /// <summary>
        /// The immediate word.
        /// </summary>
        public ushort Immediate;

        public int? OriginalInstruction;

        /// <summary>
        /// Gets the immediate word, sign-extended.
        /// </summary>
        public int SignExtendedImmediate
        {
            get
            {
                return (short)Immediate;
            }
        }
        /// <summary>
        /// The shift amount for shift instructions.
        /// </summary>
        public byte sa;

        /// <summary>
        /// The function code for a register-based instuction.
        /// </summary>
        public FunctionCode FunctionCode;

        /// <summary>
        /// The target for jump instructions.
        /// </summary>
        public int Target;


        public void Decode(uint word)
        {
            OpCode = (OpCode)((uint)word >> 26);

            if (((int)OpCode & 0x3E) != 2)
            {
                // This is not a jump instruciton, so there are source registers.
                Rt = (int)((word >> 16) & 0x1F);
                Rs = (int)((word >> 21) & 0x1F);

                // If this is a register format instruction, there is a destination register and a function code.
                if (OpCode == OpCode.Register)
                {
                    FunctionCode = (FunctionCode)(word & 0x3F);
                    Rd = (int)((word >> 11) & 0x1F);

                    // If this is a shift instruction, there is also a shift amount field.
                    if ((int)OpCode < 4)
                    {
                        sa = (byte)((word >> 6) & 0x1F);
                    }
                }
                else
                {
                    // If this is not a register format insruction, there is also an immediate half-word.

                    Immediate = (ushort)word;
                }
            }
            else
            {
                // If this is a jump instruction (OpCode = 00001?b), encode the target
                Target = (int)word & 0x3FFFFFF;

                if ((word & 0x2000000) != 0)
                {
                    Debugger.Break();
                    // If this is a negative branch instruction, so make the integer negative.
                    Target = unchecked((int)((uint)Target | 0xFC000000));
                }
            }
        }


        public override string ToString()
        {
            var code = OpCode == OpCode.Register ? FunctionCode.ToString() : OpCode.ToString();

            return string.Format("{0}, {1}, {2}, {3}", code, (Register)Rs, (Register)Rt, Immediate);
        }
    }
}
